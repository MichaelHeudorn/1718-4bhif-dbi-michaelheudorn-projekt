#BandProjekt Neo4j

Dieses Projekt speichert Bands in einer Neo4J Graph Datenbank ab, und gibt diese mit Spring an einer REST-Schnittstelle aus

##Systemvorraussetzungen
Um das Projekt auszuführen wird mindestens Java 1.8 benötigt. Des Weiteren wird eine Neo4J Datenbank benötigt (am besten in einem Docker Client). Man muss sich zu der Neo4J Datenbank über den Bolt Port 7687 verbinden können.